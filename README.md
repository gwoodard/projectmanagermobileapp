# Project Manager Mobile Application - Cross Platform #

### **About:** ###

This repo represents the source code for a HW assignment about Cross Platform Application Development. The application is a Project Manager application that will allow users to input Project Dates (start and end dates), meeting date, choose a PM's name then input meeting notes. The application is to be saved to a database backend. This was a group project. A research paper accompanies this repo (MobileProjectMonitorApp.docx) and below is a snippet of the research:

*For this application we are using a cross development approach. Instead of developing for just one platform at a time, we are using a cloud based platform that utilizes web programming technologies such as HTML, JavaScript and CSS. This allows you to design the front end from a drap-and-drop interface, program the logic using both drag-and-drop interface and JavaScript, then implement CSS to tweak the design and use PhoneGap to process and compile the project into a functioning application for iOS, Android, Windows Phone and a standalone HTML website. This is all done from the Appery.io platform without the need for seperate programmers for each platform. This platform single handedly compiles for you all the platform specific binaries (final application package) that is ready for submission to each respective app store or available to launch on a web hosting account. Also this platform gives you the option to export the project to Eclipse for Android development, XCode for iOS development and Visual Studio for C#/Windows Development*

### **IDE:**
Appery.io

### **Input:** ###
Project Start Date, Project End Date, Project Manager's Name (drop down box), Meeting Date and Text box for meeting notes

### **Language:** ###
Cross Platform Mobile Application for iOS and Android using HTML, JavaScript, CSS, PhoneGap